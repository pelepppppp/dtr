import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  count = 0;
  people = [];
  saved = [];
  saveRecordVisible = false;
  newRecordVisible = false;
  savedTableButtonVisible = false;
  hideTableButtonVisible = false;
  savedTableVisible = false;
  formView = true;
  removeTD = true;

  myDate() {
    var date = new Date();
    var number = date.getMinutes().toString();
    var time = null;
    if (number.length == 2) {
      time = (date.getHours() % 12 || 12) + ":" + date.getMinutes();
    } else {
      time = (date.getHours() % 12 || 12) + ":" + "0" + date.getMinutes();
    }
    return time;
  }

  timeIn(value: any) {
    const date = new Date();
    const dd = String(date.getDate()).padStart(2, '0');
    const mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy = date.getFullYear();
    const today = mm + '/' + dd + '/' + yyyy;
    if (!value.fname || !value.lname) {
    } else {
      var person = { name: value.lname + ", " + value.fname, date: today, timein: this.myDate(), timeout: "0:00" };
      this.people.push(person);
    }
  }

  timeOut(index) {
    this.people[index].timeout = this.myDate();
    this.count += 1;
    this.saveView();
  }

  remove(index) {
    if (this.people[index].timeout != "0:00") {
      this.count -= 1;
    }
    for (var i = 0; i <= index; i++) {
      if (i == index) {
        this.people.splice(index, 1);
      }
    }
    this.saveView();
  }

  showTable() {
    this.savedTableVisible = true;
    this.savedTableButtonVisible = false;
    this.hideTableButtonVisible = true;
  }

  hideTable() {
    this.savedTableVisible = false;
    this.hideTableButtonVisible = false;
    this.savedTableButtonVisible = true;
  }

  saveRecord() {
    this.saved.push(this.people)
    this.newRecordVisible = true;
    this.saveRecordVisible = false;
    this.removeTD = false;
    this.formView = false;
    this.savedTableButtonVisible = true;
    this.hideTableButtonVisible = false;
    this.savedTableVisible = false;
  }

  newRecord() {
    this.people = [];
    this.newRecordVisible = false;
    this.removeTD = true;
    this.formView = true;
  }
  saveView() {
    if (this.count == this.people.length) {
      this.saveRecordVisible = true;
      this.count = 0;
    }
  }

}





