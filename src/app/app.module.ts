import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutusComponent
  ],
  imports: [
    RouterModule.forRoot([
      {path:'', component: HomeComponent},
      {path:'aboutus', component: AboutusComponent}
    ]),
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],

  providers: [

  ],

  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }




